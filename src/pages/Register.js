import { useState, useEffect,useContext } from 'react';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){

    // State hooks tto store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    const {user, setUser} = useContext(UserContext);
    

    // console.log(email);
    // console.log(password1);
    // console.log(password2);
    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, password1, password2]);


    // Function to simulate user registration
    function registerUser(e) {
        // Prevents page redirection via form submission
        e.preventDefault();

            fetch(`http://localhost:4000/users/checkEmail`, {
              method: "POST",
              headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                email: email
              })
            })
              .then(res => res.json())
              .then(data => {
                if (data) {
                  Swal.fire({
                    title: "Email already registered",
                    icon: "error",
                    text: "Please try again."
                  });
                } else {
                  fetch(`http://localhost:4000/users/register`, {
                    method: "POST",
                    headers: {
                      'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                      firstName: firstName,
                      lastName: lastName,
                      email: email,
                      mobileNo: mobileNo,
                      password: password1
                    })
                  })
                    .then(res => res.json())
                    .then(data => {
                      if (data) {
                        Swal.fire({
                          title: "Registration successful",
                          icon: "success",
                          text: "Welcome to Zuitt!"
                        });
                      } else {
                        Swal.fire({
                          title: "Registration unsuccessful",
                          icon: "error",
                          text: "Please try again."
                        });
                      }
                    })
                }
              })
          }

            return(

            	// Redirects authenticated user to courses page
                (user.id !==null) ?
                    <Navigate to="/courses"/>
                    :
                        <Form onSubmit={(e) => registerUser(e)}>

                            <Form.Group controld="userEmail">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter First Name"
                                    value={firstName}
                                    onChange={e => setFirstName(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controld="userEmail">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Enter Last Name"
                                    value={lastName}
                                    onChange={e => setLastName(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="userEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    placeholder="Enter email"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)} 
                                    required
                                />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group controld="userEmail">
                                <Form.Label>Mobile Number</Form.Label>
                                <Form.Control
                                    type="mobileNumber"
                                    placeholder="Enter your Mobile Number"
                                    value={mobileNo}
                                    onChange={e => setMobileNo(e.target.value)}
                                    required
                                />
                            </Form.Group>


                            <Form.Group controlId="password1">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Password"
                                    value={password1}
                                    onChange={e => setPassword1(e.target.value)} 
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="password2">
                                <Form.Label>Verify Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    placeholder="Verify Password" 
                                    value={password2}
                                    onChange={e => setPassword2(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            {isActive ? 
                                <Button variant="primary" type="submit" id="submitBtn">
                                    Submit
                                </Button>
                                :
                                <Button variant="primary" type="submit" id="submitBtn" disabled>
                                    Submit
                                </Button>
                            }
                        </Form>

            )
        }
